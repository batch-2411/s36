// Controllers contains the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the "require" directive to allow access to the "Task" model which allows us to access methods to perform CRUD operations
// Allows us to use the contents of the "task.js" file in the "models" folder

const Task = require("../models/task");

// Controller function for GETTING ALL THE TASKS
// Defines the functions to be used in the "taskRoute.js" file and export these functions

module.exports.getAllTasks = () => {

	// The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to client/Postman

	// model.mongoose method
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {

	// Creates a task object based on the Mongoose Model "task"
	let newTask = new Task({
		// Sets the name property with the value received from the client
		name: requestBody.name
	});

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	});
};

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, requestBody) => {

	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			result.name = requestBody.name;
			return result.save().then((updatedTask, saveErr) => {
				if(saveErr) {
					console.log(saveErr);
				} else {
					return updatedTask;
				}
			})
		}
	})
}

// ACTIVITY

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((task, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return task;
		}
	})
}

module.exports.updateTaskComplete = (taskId) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			result.status = "complete";
			return result.save().then((updatedTask, saveErr) => {
				if(saveErr) {
					console.log(saveErr);
				} else {
					return updatedTask;
				}
			})
		}
	})
}