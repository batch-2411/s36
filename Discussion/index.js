// Set up the dependencies
const express = require('express');
const mongoose = require('mongoose');
// This allows us to use all routes defined in  "taskRoute.js"
const taskRoutes = require('./routes/taskRoutes');

// Server Setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database Connection
// Connecting to MongoDB Atlas
mongoose.set('strictQuery', false);
mongoose.connect('mongodb+srv://admin123:admin098@cluster0.khqxuap.mongodb.net/s36?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', console.error.bind(console, "Connection Error \n"));
db.once('open', () => console.log('Connected to MongoDB Atlas!'));

// Add the task route
// Allows all the task routes created in the "taskRoutes.js" file to use "/tasks" 
app.use('/tasks', taskRoutes);
//http://localhost:4000


app.listen(port, () => console.log(`Server is listening port ${port}`));